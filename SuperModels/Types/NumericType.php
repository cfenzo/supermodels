<?php namespace SuperModels\Types;

class NumericType extends BaseType {

    static function transformIn($value, $conf){
        return intval($value);
    }
    static function transformOut($value, $conf){
        return intval($value);
    }

    static function getDefault($conf, $value = null){
        return !is_numeric($value)?0:$value;
    }

    static function validate($conf, $value){
        if(!is_numeric($value));
        if(isset($conf['max']) && $value > $conf['max']);
        if(isset($conf['min']) && $value < $conf['min']);
    }
}