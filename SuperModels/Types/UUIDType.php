<?php namespace SuperModels\Types;

class UUIDType extends BaseType {
    static $inFunction = 'uuid_text_to_binary';
    static $outFunction = 'uuid_binary_to_text';
    static $whereValueFunction = 'uuid_text_to_binary';

    static function getDefault($conf, $value = null){
        if($value == true){
            $id = getDatabase()->one('SELECT uuid() as id');
            return $id['id'];
        }
        return null;
    }
}