<?php namespace SuperModels\Types;

interface TypeInterface {
    static function transformIn($value, $conf);
    static function transformOut($value, $conf);
}