<?php namespace SuperModels\Types;

class TimestampType  extends BaseType {

    static function getDefault($conf, $value = ''){
        if($value == true){
            return time();
        }
        return '';
    }
}