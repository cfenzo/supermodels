<?php namespace SuperModels\Types;

use SuperModels\Exceptions\JSONTypeValidationException;

class JSONType extends StringType {

    static function transformIn($value, $conf){
        return is_array($value)?json_encode($value):$value;
    }
    static function transformOut($value, $conf){
        return static::isValidJSON($value)?json_decode($value, true):$value;
    }

    static function getDefault($conf, $value = null){
        return !is_array($value)?[]:$value;
    }

    static function validate($conf, $value){
        if($value === '' || is_null($value) || is_array($value)) return true;
        throw new JSONTypeValidationException();
    }

    static function isValidJSON($str=NULL){
        if (is_string($str)) {
            @json_decode($str);
            return (json_last_error() === JSON_ERROR_NONE);
        }
        return false;
    }
}