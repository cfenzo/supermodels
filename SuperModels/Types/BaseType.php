<?php namespace SuperModels\Types;

class BaseType implements TypeInterface {

    static $inFunction = false;
    static $outFunction = false;
    static $whereColFunction = false;
    static $whereValueFunction = false;

    static function transformIn($value, $conf){
        return $value;
    }
    static function transformOut($value, $conf){
        return $value;
    }

    static function getDefault($conf, $value = null){
        return $value;
    }

    static function validate($conf, $value){}
}