<?php namespace SuperModels\Types;

class StringType extends BaseType {

    static function getDefault($conf, $value = ''){
        return !is_string($value)?'':$value;
    }

    static function validate($conf, $value){
        if(!is_string($value));
        if(isset($conf['max']) && count($value) > $conf['max']);
        if(isset($conf['min']) && count($value) < $conf['min']);
    }
}