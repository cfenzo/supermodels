<?php namespace SuperModels;

class QueryBuilder {

    const TYPE_SELECT = 'SELECT';
    const TYPE_INSERT = 'INSERT';
    const TYPE_DELETE = 'DELETE';
    const TYPE_UPDATE = 'UPDATE';

    private $query = [
        'type'=>false,
        'table'=>false,
        'columns'=>[],
        'values'=>[],
        'where'=>[],
        'group'=>[],
        'offset'=>false,
        'limit'=>false,
        'order'=>[]
    ];

    private $ai = 0;

    private function _set($key, $value){
        $this->query[$key] = $value;
        return $this;
    }
    private function _append($key, $value){
        $this->query[$key][] = $value;
        return $this;
    }

    function select($tableName){
        return $this->_set('type', static::TYPE_SELECT)->_set('table',$tableName);
    }

    function insert($tableName){
        return $this->_set('type', static::TYPE_INSERT)->_set('table',$tableName);
    }
    function delete($tableName){
        return $this->_set('type', static::TYPE_DELETE)->_set('table',$tableName);
    }
    function update($tableName){
        return $this->_set('type', static::TYPE_UPDATE)->_set('table',$tableName);
    }

    function set($col, $value, $inFunction = false){
        $placeholder = ':'.$col;
        return $this->_append(
            'columns',
            $col . ' = ' . ($inFunction!==false?$inFunction .'('.$placeholder.')':$placeholder)
        )->value($col, $value);
    }
    function col($col, $alias = false, $outFunction = false){
        return $this->_append('columns', ($outFunction!==false ? $outFunction .'('.$col.')':$col) . ($alias?' as '.$alias:($outFunction?' as '.$col:'')));
    }
    function cols($cols){
        foreach($cols as $col){
            $this->col($col[0], $col[1], $col[2]);
        }
        return $this;
    }

    // TODO add WHERE IN, WHERE NOT IN ++
    function where($col, $comparator = '=', $value, $colFunction = false, $valueFunction = false){
        $placeholder = ':'.$col;
        return $this->_append('where', implode(' ', [
            ($colFunction!==false?$colFunction .'('.$col.')':$col),
            $comparator,
            ($valueFunction!==false?$valueFunction .'('.$placeholder.')':$placeholder)
        ]))->value($col, $value);
    }
    function value($col, $value){
        $this->query['values'][':'.$col] = $value;
        return $this;
    }

    function groupBy($col){
        $this->query['group'][] = $col;
    }
    function orderBy($col, $direction='ASC'){
        $this->query['order'][] = $col.' '.$direction;
    }
    function limit($limit){
        $this->query['limit'] = $limit;
    }
    function offset($offset){
        $this->query['offset'] = $offset;
    }


    function getQuery(){
        $raw = $this->query;

        $query = [];

        switch($raw['type']){
            case static::TYPE_SELECT:
                $query[] = "SELECT";
                $query[] = count($raw['columns'])?implode(', ', $raw['columns']):'*';
                $query[] = "FROM";
                $query[] = $raw['table'];
                $query = $this->addWhereQuery($query, $raw);
                $query = $this->addGroupByQuery($query, $raw);
                $query = $this->addOrderByQuery($query, $raw);
                $query = $this->addLimitQuery($query, $raw);
                $query = $this->addOffsetQuery($query, $raw);
                break;
            case static::TYPE_INSERT:
                $query[] = "INSERT INTO";
                $query[] = $raw['table'];
                $query = $this->addSetQuery($query, $raw);
                break;

            case static::TYPE_UPDATE:
                $query[] = "UPDATE";
                $query[] = $raw['table'];
                $query = $this->addSetQuery($query, $raw);
                $query = $this->addWhereQuery($query, $raw);
                break;

            case static::TYPE_DELETE:
                $query[] = "DELETE";
                $query[] = $raw['table'];
                $query = $this->addWhereQuery($query, $raw);
                break;
            default:
        }

        return implode(' ', $query);
    }

    private function addWhereQuery($query, $raw){
        if(count($raw['where'])){
            $query[] = 'WHERE';
            $query[] = implode(' AND ', $raw['where']);
        }
        return $query;
    }
    private function addSetQuery($query, $raw){
        if(count($raw['columns'])) {
            $query[] = "SET";
            $query[] = implode(', ', $raw['columns']);
        }
        return $query;
    }
    private function addLimitQuery($query, $raw){
        if($raw['limit'] !== false){
            $query[] = "LIMIT";
            $query[] = $raw['limit'];
        }
        return $query;
    }
    private function addGroupByQuery($query, $raw){
        if(count($raw['group'])){
            $query[] = "GROUP BY";
            $query[] = implode(', ', $raw['group']);
        }
        return $query;
    }
    private function addOrderByQuery($query, $raw){
        if(count($raw['order'])){
            $query[] = "ORDER BY";
            $query[] = implode(', ', $raw['order']);
        }
        return $query;
    }
    private function addOffsetQuery($query, $raw){
        if($raw['offset'] !== false){
            $query[] = "OFFSET";
            $query[] = $raw['offset'];
        }
        return $query;
    }

    function getValues(){
        return $this->query['values'];
    }
}