<?php namespace SuperModels;

// loosely based on https://github.com/ArthurD/php-crud-model-class/
use SuperModels\Types\NumericType;
use SuperModels\Types\UUIDType;

class SuperModel {

    public static $scheme = [];
    public static $tableName;
    public static $primary;
    public static $dataColumn;

    protected $properties = array();
    protected $is_raw = true;

    public function __construct($data_array = [], $is_raw = true) {
        if(is_array($data_array)){
            $this->setArr($data_array, true);
        }
        $this->is_raw = $is_raw;
    }

    ############################################################
    ### MAGIC METHODS - Constructor / Getter / Setter
    ############################################################

    public function __get($key) {
        if (array_key_exists($key, $this->properties)) {
            return $this->properties[$key];
        }
        if(isset(static::$dataColumn) && isset(static::$scheme[static::$dataColumn])){
            $dataColumn = $this->__get(static::$dataColumn);
            if(array_key_exists($key, $dataColumn)) return $dataColumn[$key];
        }
        return null;
    }

    public function __set($key, $value) {
        if(isset(static::$scheme[$key])){
            // TODO add typecheck/validation, let type validation throw exception?
            call_user_func(static::$scheme[$key]['type'].'::validate', static::$scheme[$key], $value);

            return $this->properties[$key] = $value;
        }

        // support for "datacolumn", all non-defined properties will be added here
        if(isset(static::$dataColumn)){
            if(!isset(static::$scheme[static::$dataColumn]) || !is_array(static::$scheme[static::$dataColumn])){
                $this->properties[static::$dataColumn] = $this->getTypeDefault(static::$dataColumn);
            }
            return $this->properties[static::$dataColumn][$key] = $value;
        }

        return null;
    }

    public function setArr($rawData, $defaults = false){
        foreach($rawData as $colName => $value){
            $this->$colName = $value;
        }
        if($defaults){
            foreach(static::$scheme as $colName => $conf){
                if(!isset($this->properties[$colName])){
                    $this->$colName = static::getTypeDefault($colName);
                }
            }
        }

        return $this;
    }

    ############################################################
    ### INSTANCE METHODS - Validation, Load, Save
    ############################################################

    public function validate() {
        // TODO run validation on all fields, eg: typeClass::validate($value)
        return true;
    }

    public function exists() {
        if(isset($this->properties) && isset($this->properties['id'])) {
            return true;
        } else {
            return false;
        }
    }

    static function load($primary){
        return static::one([
            [static::$primary, '=', $primary]
        ]);
    }

    static function one($conditions){
        $qb = new QueryBuilder();

        $qb->select(static::$tableName)
            ->cols(static::getSelectColumns());

        foreach($conditions as $c){
            $qb->where($c[0], $c[1], $c[2], static::getTypeFunc('whereColFunction',$c[0]), static::getTypeFunc('whereValueFunction',$c[0]));
        }

        $raw = getDatabase()->one($qb->getQuery(), $qb->getValues());
        if(!$raw) return false;

        return static::_data2model($raw);
    }
    static function all($conditions){
        $qb = new QueryBuilder();

        $qb->select(static::$tableName)
            ->cols(static::getSelectColumns());

        foreach($conditions as $c){
            $qb->where($c[0], $c[1], $c[2], static::getTypeFunc('whereColFunction',$c[0]), static::getTypeFunc('whereValueFunction',$c[0]));
        }

        return array_map(static::class.'::_data2model',
            getDatabase()->all($qb->getQuery(), $qb->getValues()));
    }

    private static function _data2model($raw){
        foreach($raw as $columnName => $value){
            $raw[$columnName] = isset(static::$scheme[$columnName]) && is_callable(static::$scheme[$columnName]['type'].'::transformOut')?call_user_func(static::$scheme[$columnName]['type'].'::transformOut',$value, static::$scheme[$columnName]):$value;
        }
        return new static($raw, false);
    }

    static function count($column, $conditions = [], $groupBy = false){
        $qb = new QueryBuilder();

        $qb->select(static::$tableName)
            ->col($column, 'count', 'count')
            ->col($column, $column, static::getTypeFunc('outFunction', $column));

        if($groupBy) $qb->groupBy($column);

        if(is_array($conditions)){
            foreach($conditions as $c){
                $qb->where($c[0], $c[1], $c[2], static::getTypeFunc('whereColFunction',$c[0]), static::getTypeFunc('whereValueFunction',$c[0]));
            }
        }

        $raw = getDatabase()->all($qb->getQuery(), $qb->getValues());

        return array_map(function($row){
            foreach($row as $columnName => $value){
                $row[$columnName] = isset(static::$scheme[$columnName]) && is_callable(static::$scheme[$columnName]['type'].'::transformOut')?call_user_func(static::$scheme[$columnName]['type'].'::transformOut',$value, static::$scheme[$columnName]):$value;
            }
            $row['count'] = NumericType::transformOut($row['count'],[]);
            return $row;
        },$raw);
    }

    // for when you only want to update a single column
    public function updateColumn($columnName, $value){
        return $this->updateColumns([
            $columnName=>$value
        ]);
    }
    public function updateColumns($arr){
        // only run update on inserted (not raw) models
        if($this->is_raw) return false;

        // backup old values, build column list, and set new values
        $oldValues = [];
        $columns = [];
        foreach($arr as $columnName => $value) {
            $columns[] = $columnName;
            $oldValues[$columnName] = $this->$columnName;
            $this->$columnName = $value;
        }

        // try to save!
        if($this->save($columns)){
            return true;
        }

        // if save failed, reset to old values
        // TODO add log warning here
        foreach($oldValues as $columnName => $value){
            $this->$columnName = $value;
        }
        return false;
    }

    public function save($columns = false){

        $qb = new QueryBuilder();

        if($this->is_raw){
            // check if primary is not set, and not autoIncrement
            if(!isset($this->properties[static::$primary]) && (!isset(static::$scheme[static::$primary]['autoIncrement']) || static::$scheme[static::$primary]['autoIncrement'] !== true)){
                $this->__set(static::$primary, $this->getTypeDefault(static::$primary));
            }
            // insert
            $qb->insert(static::$tableName);
        }else{
            $qb->update(static::$tableName)
                ->where(static::$primary, '=', $this->__get(static::$primary), static::getTypeFunc('whereColFunction',static::$primary), static::getTypeFunc('whereValueFunction',static::$primary));
        }

        // if no columns specified, save ALL columns
        if(!$columns){
            $columns = array_keys(static::$scheme);
        }

        foreach($columns as $columnName){
            // TODO add required check
            // TODO only add fields with values
            $conf = static::$scheme[$columnName];
            $qb->set($columnName, call_user_func($conf['type'].'::transformIn',$this->__get($columnName), $conf), static::getTypeFunc('inFunction', $columnName));
        }

        if(getDatabase()->execute($qb->getQuery(), $qb->getValues())){
            $this->is_raw = false;
            return true;
        }
        return false;
    }

    static function getTypeDefault($property){
        $scheme = static::$scheme[$property];
        $def = null;

        if(isset($scheme['default'])){
            $def = (is_callable($scheme['default']))?call_user_func($scheme['default'], $scheme):$scheme['default'];
        }

        $className = $scheme['type'];
        $defaultMethod = $className.'::getDefault';
        if(class_exists($className) && is_callable($defaultMethod)){
            return call_user_func($defaultMethod, $scheme, $def);
        }

        return null;
    }

    static function getTypeFunc($funcName, $property){
        $className = static::$scheme[$property]['type'];

        if(!class_exists($className)) return false;
        if(!property_exists($className, $funcName)) return false;

        $vars = get_class_vars($className);
        return $vars[$funcName];
    }

    static function getSelectColumns(){
        $columns = [];
        foreach(static::$scheme as $columnName => $conf){
            $columns[] = [$columnName, $columnName, static::getTypeFunc('outFunction', $columnName)];
        }

        return $columns;
    }

}